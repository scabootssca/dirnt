import core

lineSplit = core.re.compile("\r?\n")
messageSplit = core.re.compile("^(:(?P<prefix>[^ ]+) +)?(?P<command>[^ ]+)( *(?P<argument> .+))?")

class User:
	def __init__(self,nick="",mask=""):
		self.nick = nick
		self.mask = mask
			
		self.voice = 0
		self.op = 0
		self.halfop = 0

class Channel:
	def __init__(self,name):
		self.name = name
		
		self.users = {}
		
	def addUser(self,nick,mask=""):
		newUser = User(nick,mask)

		# Check to see if the user has a mode
		if nick[0] in "+@%":
			if nick[0] == "+": newUser.voice = 1
			elif nick[0] == "@": newUser.op = 1
			elif nick[0] == "%": newUser.halfop = 1
			
			nick = nick[1:]
			newUser.nick = nick

		# Add the new user into the list
		self.users[nick] = newUser

	def delUser(self,nick):
		if nick in self.users: del self.users[nick]
		
class Network:
	def __init__(self,client,name):
		self.client = client
		self.name = name
		
		self.channels = {}
		
		# Connection stuff
		self.socket = 0
		self.connected = 0
		self.previousBuffer = ""

		# User information
		# Need to make this read from settings
		self.userInfo = {"nick":client.core.settings.get("Client","nick"),"realname":client.core.settings.get("Client","realname"),"username":client.core.settings.get("Client","username"),"password":self.client.networkInfo.get(self.name,"pass")}

		# Network information
		self.servers = self.client.networkInfo.get(self.name,"servers")
		self.port = self.client.networkInfo.get(self.name,"port")
		self.join = self.client.networkInfo.get(self.name,"join")
		
		self.server = 0
		self.reconnecting = 0

	def joinChannel(self,name,net=1):
		self.channels[name] = Channel(name)
		if net: self.send("JOIN "+name)
		
	def partChannel(self,name,message,net=1):
		if name in self.channels:
			del self.channels[name]
			if net: self.send("PART %s :%s"%(name,message))
		

	def reconnect(self):
		if self.client.core.settings.get("Client","reconnect") and not self.reconnecting:
			self.reconnecting = 1
			while not self.connected:
				core.time.sleep(5)
				self.connect()

	def connect(self):
		# Might be useful
		if self.connected:
			self.disconnect("Cycling servers")
			
			# Cycle to the next server
			self.server += 1
			if self.server >= len(self.servers): self.server = 0

		# Try to connect
		self.socket = core.socket.socket(core.socket.AF_INET, core.socket.SOCK_STREAM)

		try:
			#self.socket.bind(("", 0))
			self.socket.connect((self.servers[self.server], self.port))
		except core.socket.error, x:
			core.db.logError("connection","Couldn't connect to socket: %s" % x)
			
			self.socket.close()
			self.socket = None
			
			self.reconnect()
			return

		# We sucessfully connected
		self.connected = 1
		
		# Stop trying to reconnect if we were
		self.reconnecting = 0

		# Log on...
		if self.userInfo["password"]: self.send("PASS "+self.userInfo["password"])
        
		self.send("NICK "+self.userInfo["nick"])
		self.send("USER %s 0 * :%s"%(self.userInfo["username"],self.userInfo["realname"]))

	def disconnect(self,message="",disconnect=0,error=0):
		# Tell the network
		if not disconnect: self.send("QUIT :"+message,disconnect+1)
		if error: core.db.logError("connection","Disconnected: "+message)
		
		# End the socket
		self.socket = 0
		self.connected = 0
		
	def send(self,message,disconnect=0):
		# Make sure we're connected
		if self.socket == 0:
			core.db.logError("connection","Not connected")
			#self.reconnect()
			return

		try:
			# Log the line
			core.db.logSocket("out",message)
			
			self.socket.send(message + "\r\n")
			# Debug here
		except core.socket.error, x:
			self.disconnect("Connection reset by peer. (Error Sending)",disconnect,error=1)
			self.reconnect()
	
	def recv(self):
		try:
			data = self.socket.recv(16384)
		except core.socket.error, x:
			# The server hung up.
			self.disconnect("Connection reset by peer. (Error Recieving)",error=1)
			self.reconnect()
			return

		if not data:
			# Read nothing: connection must be down.
			self.disconnect("Connection reset by peer. (Nothing Recieved)",error=1)
			self.reconnect()
			return

		lines = lineSplit.split(self.previousBuffer + data)

		# Save the last, unfinished line.
		self.previousBuffer = lines[-1]
		lines = lines[:-1]


		for line in lines:
			if not line: continue

			# Log the line
			core.db.logSocket("in",line)

			prefix = None
			command = None
			arguments = None

			m = messageSplit.match(line)

            
			if m.group("prefix"):
				prefix = m.group("prefix")

			if m.group("command"):
				command = m.group("command").lower()

			if m.group("argument"):
				a = m.group("argument").split(" :", 1)
				arguments = a[0].split()
				if len(a) == 2: arguments.append(a[1])
				
			# Handle it
			core.netIn.handle(self.client.core,prefix,command,arguments,self)
		
class Main:
	def __init__(self):
		self.networks = {}
		
		self.networkInfo = core.inio.Parser("networks.ini")
		
	# Network\Channel IO
	def joinNetwork(self,networkName):
		# If we do not have that network in our database make an entry
		if not networkName in self.networkInfo.sections:
			core.db.logError("client","Unknown network: %s"%networkName)
			tmp = core.inio.Section(networkName)
			tmp.values = {"servers":[networkName],"join":[""],"pass":"master","port":6667}
			tmp.order = ["servers","join","pass","port"]
			self.networkInfo.sections[networkName] = tmp
		
		network = Network(self,networkName)
		self.networks[networkName] = network
		network.connect()

	def partNetwork(self,networkName,message=""):
		if networkName in self.networks:
			network = self.networks[networkName]
			network.disconnect(message)
			del self.networks[networkName]
			
	def joinChannel(self,channelName,networkName):
		if networkName in self.networks:
			self.networks[networkName].joinChannel(channelName)
			
	def partChannel(self,channelName,networkName,message=""):
		if networkName in self.networks:
			self.networks[networkName].partChannel(channelName,message)

	# Wait and recieve
	def processData(self, sockets):
		for socket in sockets:
			for network in self.networks.values():
				if socket == network.socket:
					network.recv()

	def processOnce(self, timeout=0):
		sockets = map(lambda x: x.socket, self.networks.values())
		sockets = filter(lambda x: x != None, sockets)
		if sockets:
			(i, o, e) = core.select.select(sockets, [], [], timeout)
			self.processData(i)
		else:
			core.time.sleep(timeout)

	def processForever(self, timeout=0.2):
		while 1:
			self.processOnce(timeout)
			
			# If the core quit running then quit the loop
			if not core.cCore.running: break
	
	
