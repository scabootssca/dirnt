moduleName = "uiserver"
ramKeys = ["uiServer"]

import socket, select, time, thread, random

# From server
# WELCOME == welcome message (We're connected)
# MSG [nick] [type] [message] == New line incoming
# BYE == Acknowledge client disconnect

# From clients
# SEND [message] == Send this
# QUIT == Disconnect client

class Client:
	def __init__(self,server,client,address):
		self.server = server
		self.client = client
		self.address = address
	
	def send(self,msg):
		self.client.send(msg)
	
	def run(self):
		server = self.server
		client = self.client
		address = self.address
		
		print "Recieved connection from [%s:%s]"%(address[0],address[1]);
		client.send("WELCOME\r\n")
		buffer = ""
		self.running = 1
		
		while self.running:
			if not select.select([client],[],[],.3)[0]: time.sleep(.3)

			lines = (buffer+client.recv(1024)).split("\r\n")
			buffer = lines.pop(-1)
			
			for line in lines:
				if not line.strip(): continue
				print "Recieved [%s:%s]: %s"%(address[0],address[1],line)
				line = line.split()

				if line[0] == "QUIT":
					print "Disconnecting client [%s:%s]"%(address[0],address[1])
					client.send("BYE\r\n");
					self.running = 0
					break
					
				if line[0] == "OFF":
					print "Shutting down server"
					server.shutdown()
					self.running = 0
					break
					
				if line[0] == "RAW":
					server.core.client.send(line[1]," ".join(line[2:]))
				
		print "Cleaning up after client [%s:%s]"%(address[0],address[1])	
		del server.clients[address]
		client.close()
		return

class UiServer:
	def __init__(self,core):
		# Create the server and listen on the correct port
		self.core = core
		self.server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self.clients = {}
		self.running = 1
		
	def random(self):
		while 1:
			time.sleep(random.random()*5)
			if self.clients:
				random.choice(self.clients.values())[0].send("MSG %s msg %s\r\n"%(random.choice(["scabby","fried","dirnt","kramer"]),random.choice([":D","yay!!!!!!!!","ERECTION!"])))
	
	def broadcast(self,prefix,command,arguments,network):
		for client in self.clients.values():
			client.send("%s %s %s\r\n"%(prefix,command," ".join(arguments)))
	
	def run(self):
		self.server.bind(("",1666))
		self.server.listen(5)

		while self.running:
			(c,a) = self.server.accept()
			client = Client(self,c,a)
			self.clients[a] = client
			thread.start_new_thread(client.run,())
			print "Accepted client!"

		self.shutdown()
		
	def shutdown(self):
		if self.running:
			for client in self.clients.values():
				client.running = 0
			
			print "Shutting down server"
			self.server.shutdown(socket.SHUT_RDWR)
			self.server.close()
			self.running = 0


def handleNetout(core,dest,message,network): pass

def handleNetin(core,prefix,command,arguments,network):
	core.bot.ram.get(moduleName,"uiServer").broadcast(prefix,command,arguments,network)
		
def setup(core):
	uiServer = UiServer(core)
	core.bot.ram.set(moduleName,"uiServer",uiServer)
	thread.start_new_thread(uiServer.run,())
	
def cleanup(core):
	core.bot.ram.get(moduleName,"uiServer").shutdown()
	core.bot.ram.rem(moduleName,"uiServer")
	
def addHooks(core): pass
def delHooks(core): pass
