moduleName = "hey"
ramKeys = []

def handle(core,bot,source,split):
	bot.say(source,"/all Hello")

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("hey",handle,"This makes the bot say hello.")

def delHooks(core):
	core.bot.delCommandHook("hey")

