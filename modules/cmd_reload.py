moduleName = "reload"
ramKeys = []

import traceback, misc

def handleReload(core,bot,source,split):
	# Reload all modules
	if not split:
		error = []
		
		# Reload old modules
		bot.reloadAllModules()
		
		# Search for new ones
		bot.loadAllModules()
		
		bot.say(source,"Reloaded all modules sucessfully.")
		
	# Reload netin
	elif split[0] == "netin":
		import netin
		reload(netin)
		bot.say(source,"Reloaded netin.")
	# Reload netout
	elif split[0] == "netout":
		import netout
		reload(netout)
		bot.say(source,"Reloaded netout.")
		
	# Reload clock
	#elif split[0] == "clock":
	#	import clock as clockModule
	#	reload(clockModule)
	#	
	#	# Need to make this make a new clock and start timers with a length of how long they had left
	#	
	#	bot.say(source,"Reloaded clock.")
		
	# Reload bot
	elif split[0] == "bot":
		import bot as botModule
		reload(botModule)
		
		old = core.bot
		
		try:
			core.bot = botModule.Bot(core)
			bot.say(source,"Reloaded bot.")
		except:
			core.bot = old
			traceback.print_exc()
			bot.say(source,"Error reloading bot.")
			
	# Reload ui
	elif split[0] == "ui":
		import ui as uiModule
		reload(uiModule)
		
		old = core.ui
		
		try:
			core.ui = uiModule.Ui(core)
			if len(split) > 1 and split[1] == "reset":
				old.cleanup()
				core.ui.setup()
			bot.say(source,"Reloaded ui.")
		except:
			core.bot = old
			traceback.print_exc()
			bot.say(source,"Error reloading ui.")
		
	# Reload settings
	elif split[0] == "settings":
		import settings
		reload(settings)
		bot.say(source,"Reloaded settings.")
		
	# Reload misc
	elif split[0] == "misc":
		import misc
		reload(misc)
		bot.say(source,"Reloaded misc.")
		
	# Reload db
	elif split[0] == "db":
		import db
		reload(db)
		bot.say(source,"Reloaded db.")

	# Reload db
	elif split[0] == "ram":
		import ram as ramModule
		reload(ramModule)
			
		try:
			oldStorage = core.bot.ram.storage
			core.bot.ram = ramModule.Ram(core)
			core.bot.ram.storage = oldStorage
			bot.say(source,"Reloaded ram.")
		except:
			traceback.print_exc()
			bot.say(source,"Error reloading ram.")

	# Reload client
	elif split[0] == "client":
		import client as clientModule
		reload(clientModule)

		old = core.client

		try:
			# Create a new client
			client = clientModule.Client(core)
			
			for name in old.networks:
				network = clientModule.Network(core,name)
				network.connect()
				client.networks[name] = network
		
			# Stop the old one	
			old.running = 0
			old.disconnect()
		
			# And start the new one	
			core.client = client
			core.client.mainLoop()

			bot.say(source,"Reloaded client.")
		except:
			traceback.print_exc()
			bot.say(source,"Error reloading client.")
			
	elif split[0] in core.ram.get("bot","modules"):
		bot.reloadModule(split[0])
		bot.say(source,"Reloaded module: %s"%split[0])

def handleReset(core,bot,source,split):
	if not split:
		bot.say(source,"Please choose what to reset: %s"%misc.joinList(core.ram.get("bot","modules")))
	elif not split[0] in core.ram.get("bot","modules"): bot.say(source,"That module isn't loaded.")
	else:
		bot.resetModule(split[0])
		bot.say(source,"Reset module: %s"%split[0])
	
def handleUnload(core,bot,source,split):
	if not split:
		bot.say(source,"Please choose what to unload: %s"%misc.joinList(core.ram.get("bot","modules")))
	elif not split[0] in core.ram.get("bot","modules"): bot.say(source,"That module isn't loaded.")
	else:
		bot.unloadModule(split[0])
		bot.say(source,"Unloaded module: %s"%split[0])

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("reload",handleReload,"This reloads parts of the bot.",30)
	core.bot.addCommandHook("reset",handleReset,"This resets parts of the bot.",30)
	core.bot.addCommandHook("unload",handleUnload,"This unloads parts of the bot.",30)

def delHooks(core):
	core.bot.delCommandHook("reload")
	core.bot.delCommandHook("reset")
	core.bot.delCommandHook("unload")
