moduleName = "seen"
ramKeys = []

import db, misc, time

def handle(core,bot,source,split):
	if split:
		if not db.goSQL("SELECT * FROM convolog WHERE nick = '%s' AND network = '%s' LIMIT 1",[split[0],source["network"]]):
			bot.say(source,"I have not seen %s lately."%split[0])
			return
		else:
			result = db.goSQL("SELECT UNIX_TIMESTAMP(timestamp), type, message, channel FROM convolog WHERE nick = '%s' AND network = '%s' ORDER BY recnum DESC LIMIT 1",[split[0],source["network"]])

			timeago = misc.descriptiveTimeDelta(time.time(),result[0][0])
			
			print result
			if result[0][1] == "privmsg":
				bot.say(source,"I last saw %s saying '%s' in %s %s ago."%(split[0],result[0][2],result[0][3],timeago))
			elif result[0][1] == "action":
				bot.say(source,"I last saw %s saying '*%s %s' in %s %s ago."%(split[0],split[0],result[0][2],result[0][3],timeago))
			elif result[0][1] == "join":
				bot.say(source,"I last saw %s joining %s %s ago."%(split[0],result[0][3],timeago))
			elif result[0][1] == "part":
				bot.say(source,"I last saw %s leaving %s '%s' %s ago."%(split[0],result[0][3],result[0][2],timeago))
			elif result[0][1] == "quit":
				bot.say(source,"I last saw %s quitting '%s' %s ago."%(split[0],result[0][2],timeago))
			elif result[0][1] == "nick":
				bot.say(source,"I last saw %s changing their nick to '%s' %s ago."%(split[0],result[0][2],timeago))
			else:
				bot.say(source,"I have not seen %s lately."%split[0])
	else:
		bot.say(source,"Please specify who to look for")

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("seen",handle,"This tells when someone was seen last.")
	
def delHooks(core):
	core.bot.delCommandHook("seen")
