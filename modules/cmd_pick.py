moduleName = "pick"
ramKeys = []

import random

def handle(core,bot,source,split):
	if split:
		choices = []
		for x in " ".join(split).split(" or "):
			for y in x.split(","):
				if y.strip():
					choices.append(y.strip())
	
		bot.say(source,"/all I pick: "+random.choice(choices))

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("pick",handle,"This randomly chooses from the choices you give it.")
	
def delHooks(core):
	core.bot.delCommandHook("pick")
