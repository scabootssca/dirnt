moduleName = "join/part"
ramKeys = []

def joinHandle(core,bot,source,split):
	bot.say(source,"/join %s"%" ".join(split))
	
def partHandle(core,bot,source,split):
	bot.say(source,"/part %s"%" ".join(split))

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("join",joinHandle,"Makes the bot join a channel.",30)
	core.bot.addCommandHook("part",partHandle,"Makes the bot part a channel.",30)
	
def delHooks(core):
	core.bot.delCommandHook("join")
	core.bot.delCommandHook("part")
