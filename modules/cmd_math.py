moduleName = "math"
ramKeys = ["last"]

import select, socket, urllib, re

def replaceHtmlCode(data):
	data = data.replace("&#215;","x")
	
	return data

def getData(sockfd,max=0):
	## Recieve the reply
	data = ""
	while sockfd in select.select([sockfd], [], [], 3)[0]:
		try:
			chunk = sockfd.recv(1024)
			if chunk == "": break
			data += chunk
			# Check for recieve is max
			if max:
				if len(data) >= max:
					return data
		except:
			break

	return data

def sendGet(sockfd,url,*more):
	sockfd.send("GET %s HTTP/1.0\n\n"%url)
	return getData(sockfd,max)

def doEquation(core, source, equation, returnEquation=0):
		# Replace all occurances of the variables with their values
		#if source["user"] in ram.math_vars:
		#	for val in ram.math_vars[source["user"]].keys():
		#		equation = equation.replace("$(%s)"%val,str(ram.math_vars[source["user"]][val]))

		equation = equation.replace(" ","~")
		
		# Get the last result if there is one and add that to the equation
		if source["user"].lower() in core.bot.ram.get(moduleName,"last"):
			equation = equation.replace("_",core.bot.ram.get(moduleName,"last")[source["user"].lower()])
		else:
			equation = equation.replace("_","0")
		
		connectTo = "www.google.com"
		searchAddr = "/search?hl=en&q="+urllib.quote(equation,"~")+"&btnG=Google+Search"
		searchAddr = searchAddr.replace("~","+")

		# Get the source and parse it
		sockfd = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		sockfd.connect((connectTo,80))
		data = sendGet(sockfd,searchAddr)
		sockfd.close()

		# Parse the page
		regex = re.search("calc_img.+?<b>(?P<equation>.+?) = (?P<result>.+?)</b",data,8)
		if not regex:
			return None if not returnEquation else (None,None)

		# Replace special chars
		equation = regex.group("equation").replace("<sup>","^")
		equation = equation.replace("<font size=-2> </font>",",")
		
		# Strip the tags
		equation = "".join(re.split("<.+?>",equation))
		equation = replaceHtmlCode(urllib.unquote(equation))

		# Replace special chars
		result = regex.group("result").replace("<sup>","^")
		result = result.replace("<font size=-2> </font>",",")
		
		# Strip the tags
		result = "".join(re.split("<.+?>",result))
		result = replaceHtmlCode(urllib.unquote(result))
		
		if returnEquation:
			return equation,result
		else:
			return result

def handle(core,bot,source,split):
	if not split: return
	
	#setVariable = re.match("^(?P<var>[a-zA-Z\_]+) ?= ?(?P<value>.+)$"," ".join(split))
	#if setVariable:
		#if not source["user"] in include.ram.math_vars: include.ram.math_vars[source["user"]] = {}
		#result = doEquation(source, "(%s)+0"%setVariable.group("value"))
		

		#if result == None:
			#bot.say(source, "Unable to set that variable (Invalid Value)")
			#return
		
		#if len(setVariable.group("var")) > 15:
			#bot.say(source, "That variable name is over the max length of 15 chars")
			#return
		
		#include.ram.math_vars[source["user"]][setVariable.group("var")] = result
		#bot.say(source,"Variable '%s' set to '%s'"%(setVariable.group("var"),result))
		
		#return
		
	#elif split[0] == "list":
		#if source["user"] in include.ram.math_vars and len(include.ram.math_vars[source["user"]]): bot.say(source,"You have set "+include.extras.joinList(include.ram.math_vars[source["user"]].keys()))
		#else: bot.say(source,"You have not set any variables")
		#return


	#elif include.re.match("^(unset|del) (all|[a-zA-Z\_]+)$"," ".join(split)):
		#if split[1] == "all":
			#include.ram.math_vars[source["user"]] = {}
			#bot.say(source,"All variables unset")
			
		#elif split[1] in include.ram.math_vars[source["user"]]:
			#del include.ram.math_vars[source["user"]][split[1]]
			#bot.say(source,"%s was unset"%split[1])
			
		#else:
			#bot.say(source,"You have not set that variable, so you can't unset it")
			
		#return
	
	# If we just asked what was in _ then tell us
	if split[0] == "_":
		if source["user"].lower() in core.bot.ram.get(moduleName,"last"):
			bot.say(source,"/all _ = %s"%core.bot.ram.get(moduleName,"last")[source["user"].lower()])
		else:
			bot.say(source,"/all _ = 0")
	else:
		#DOIT
		result = doEquation(core, source, "%s"%" ".join(split))

		if result == None:
			bot.say(source,"/all Google can't do that")
			return
		
		# Store the result
		core.bot.ram.get(moduleName,"last")[source["user"].lower()] = result
		
		bot.say(source,"/all "+" ".join(split)+" = "+result)

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core):
	core.bot.ram.set(moduleName,"last",{})
	
def cleanup(core):
	core.bot.ram.rem(moduleName,"last")

def addHooks(core):
	core.bot.addCommandHook("math",handle,"This does mathimatical statistical equations.")
	core.bot.addCommandHook("convert",handle,"This converts shit to other shit.")

def delHooks(core):
	core.bot.delCommandHook("math")
	core.bot.delCommandHook("convert")

