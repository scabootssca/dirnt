moduleName = "life"
ramKeys = ["timer","sessionid"]

import db, time, misc

def logLife(core):
	db.goSQL("UPDATE session SET life = %s WHERE recnum = %s;",[time.time()-core.ram.get("clock","birth"),core.bot.ram.get(moduleName,"sessionid")])

def handle(core,bot,source,split):
	bot.say(source,"/all I have been running for %s"%misc.descriptiveTimeDelta(time.time(),core.ram.get("clock","birth")))

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass

def setup(core): 
	db.goSQL("INSERT INTO session (birth) VALUES (FROM_UNIXTIME(%s));",[core.ram.get("clock","birth")])
	core.bot.ram.set(moduleName,"sessionid",db.goSQL("SELECT COUNT(recnum) FROM session;")[0][0])
	core.bot.ram.set(moduleName,"timer",core.clock.startTimer(600,logLife,[core],1))

def cleanup(core): 
	# Update the life one more time
	logLife(core)

	# And cleanup
	core.clock.stopTimer(core.bot.ram.get(moduleName,"timer"))

def addHooks(core):
	core.bot.addCommandHook("life",handle,"This returns how long the bot has been running.")
	
def delHooks(core):
	core.bot.delCommandHook("life")

