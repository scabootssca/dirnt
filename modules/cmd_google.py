moduleName = "google"
ramKeys = []

import socket, urllib, select, re

def getData(sockfd,max=0):
	""" This recieves all the data from a socket """
	data = ""
	while sockfd in select.select([sockfd], [], [], 3)[0]:
		try:
			chunk = sockfd.recv(1024)
			if chunk == "": break
			data += chunk
			# Check for recieve is max
			if max:
				if len(data) >= max:
					return data
		except:
			break

	return data

def sendGet(sockfd,url,*more):
	""" This sends a GET to a socket and returns the reply """
	sockfd.send("GET %s HTTP/1.0\n\n"%url)
	return getData(sockfd,max)

def handleGoogle(core,bot,source,split):
	safe = 1
	
	# Make a socket and get the result
	sockfd = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	sockfd.connect(("www.google.com",80))
	data = sendGet(sockfd,"/search?hl=en&q="+urllib.quote("+".join(split),"+")+"&btnG=Google+Search&safe=%s"%("active" if safe else "false"))
	sockfd.close()
	
	# Parse the result
	result = re.compile("\"(?P<link>[^\"]+?)\" class=l",re.M).search(data)
	if result: result = urllib.unquote(result.group("link").strip()).replace(" ","%20")
	else:
		# If there was no result then check for a define: result
		if not result: result = re.compile("<li>(?P<link>[^<]+?)<",re.M).search(data)
		if result: result = "Definition: "+urllib.unquote(result.group("link").strip())

	# And return it
	if not result: bot.say(source,"/all No results found")
	else: bot.say(source, "/all "+result)

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("google",handleGoogle,"This searches google and returns the result.")

def delHooks(core):
	core.bot.delCommandHook("google")

