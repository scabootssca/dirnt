moduleName = "clock"
ramKeys = []

import misc

def handle(core,bot,source,split):
	if not split:
		bot.say(source,"Timers are: %s"%misc.joinList(core.bot.ram.get("clock","timers")))
	elif misc.isNumber(split[0]):
		if not int(split[0]) in core.bot.ram.get("clock","timers"): bot.say(source,"That timer does not exit.")
		else:
			timer = core.bot.ram.get("clock","timers")[int(split[0])]
			bot.say(source,"Timer info: id:%s, length:%s, repeat:%s/%s, function:%s, args:[%s]"%(timer.id,timer.length,timer.iteration,timer.repeat,timer.function,misc.joinList(timer.args)))
	elif split[0] == "stop":
		if len(split) == 1 or not misc.isNumber(split[1]): bot.say(source,"Please pick a timer to stop.")
		elif not int(split[1]) in core.bot.ram.get("clock","timers"): bot.say(source,"That timer does not exit.")
		else:
			core.clock.stopTimer(int(split[1]))
			bot.say(source,"Timer %s stopped."%split[1])

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("clock",handle,"This can manage the bots clock and timers.")

def delHooks(core):
	core.bot.delCommandHook("clock")

