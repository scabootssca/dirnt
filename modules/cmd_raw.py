moduleName = "raw"
ramKeys = []

def handle(core,bot,source,split):
	core.client.send(source["network"]," ".join(split))

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("raw",handle,"This makes the bot send a custom message to the server.",30)

def delHooks(core):
	core.bot.delCommandHook("raw")
