moduleName = "upload"
ramKeys = ["timer","announced"]
NOLOAD
import random, db, urllib


def deleteAnnouncedUpload(recnum):
	db.goSQL("DELETE FROM uploadwaiting WHERE recnum = '%s'",[recnum],"uploads","uploader","theuploader")

def uploadCheck(core):
	# Check for finished uploads
	for result in db.goSQL("SELECT uploadwaiting.recnum AS recnum,files.filename AS filename,uploadwaiting.nick AS nick,uploadwaiting.channel AS channel,uploadwaiting.network AS network FROM files, uploadwaiting WHERE files.recnum = uploadwaiting.fileid AND uploadwaiting.uploaded = 1",[],"uploads","uploader","theuploader"):
		# If we did not already announce it
		if not result[0] in core.bot.ram.get(moduleName,"announced"):
			# Announce it
			core.bot.ram.get(moduleName,"announced")[result[0]] = 1
			core.bot.say({"user":result[2],"channel":result[3],"network":result[4]},"/all %s uploaded: http://upload.scabootssca.dyndns.org/files/%s"%(result[2],urllib.quote(result[1])))
			
			# Delete this from the upload listing in 20 seconds
			core.clock.startTimer(20,deleteAnnouncedUpload,[result[0]])
			db.goSQL("DELETE FROM uploadwaiting WHERE recnum = '%s'",[result[0]],"uploads","uploader","theuploader")
	
	# Delete expired upload links that are more than 1200 seconds (20 minutes) old
	db.goSQL("DELETE FROM uploadwaiting WHERE UNIX_TIMESTAMP(timestamp) < UNIX_TIMESTAMP(NOW())-1200",[],"uploads","uploader","theuploader")
	

def handleUpload(core,bot,source,split):
	uploadkey = random.random()
	db.goSQL("INSERT INTO uploadwaiting (uploadkey, nick, channel, network) VALUES ('%s', '%s', '%s', '%s')",[uploadkey,source["user"],source["channel"],source["network"]],"uploads","uploader","theuploader")
	
	bot.say(source,"/pm Upload here: http://upload.scabootssca.dyndns.org/u.php?uploadkey=%s you have 20 minutes"%uploadkey)

def handleFiles(core,bot,source,split):
	if not split: bot.say(source,"/all The files are here: http://upload.scabootssca.dyndns.org/")
	else: bot.say(source,"/all %s's files are here: http://upload.scabootssca.dyndns.org/?nick=%s"%(split[0],split[0]))
	
def setup(core):
	core.bot.ram.set(moduleName,"timer",core.clock.startTimer(5,uploadCheck,[core],0))
	core.bot.ram.set(moduleName,"announced",{})
	
def cleanup(core):
	core.clock.stopTimer(core.bot.ram.get(moduleName,"timer").id)
	core.bot.ram.rem(moduleName,"timer")
	core.bot.ram.rem(moduleName,"announced")

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass

def addHooks(core):
	core.bot.addCommandHook("upyours",handleUpload,"This makes the bot send an upload link.")
	core.bot.addCommandHook("files",handleFiles,"This sends a link to view all or a specific persons files.")

def delHooks(core):
	core.bot.delCommandHook("upyours")
	core.bot.delCommandHook("files")

