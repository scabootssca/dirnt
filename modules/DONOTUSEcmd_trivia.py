moduleName = "trivia"
ramKeys = []

import random, time, db, re, misc

def rankText(number):
	if number == 1: return "1st"
	elif number == 2: return "2nd"
	elif number == 3: return "3rd"
	else: return "%sth"%number

def getKey(source):
	return source["network"]+source["channel"]

def triviaRunning(source,core):
	if getKey(source) in core.bot.ram.get(moduleName,"trivia"): return 1
	else: return 0

def resetTriviaStats(source,core):
	core.bot.ram.get(moduleName,"trivia")[getKey(source)] = {}
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]

	# Set ram stuffs
	trivia["roundstarted"] = 0
	trivia["currentround"] = 1
	trivia["rounds"] = 0
	trivia["streak"] = ["",0]
	trivia["nextquestiontimer"] = 0
	
	# This is set in startQuestion
	trivia["answers"] = []
	trivia["start"] = 0
	trivia["timers"] = []
		
	# Store the winners
	trivia["winners"] = {}

def stopTrivia(core,source):
	if triviaRunning(source,core):
		trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
		for timer in trivia["timers"]: core.clock.stopTimer(timer.id)
		if trivia["nextquestiontimer"]: core.clock.stopTimer(trivia["nextquestiontimer"].id)
		#resetTriviaStats(source,core)
		del core.bot.ram.get(moduleName,"trivia")[getKey(source)]

def giveSpaces(source,core):
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if not trivia["roundstarted"]: return
	
	new = ["-"*len(word) for word in trivia["answers"][0].split()]
	core.bot.say(source,"/all Hint: 02Answer space: %s"%" ".join(new))
	
def hintRandomLetters(source,core,pm=0):
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if not trivia["roundstarted"]: return
	
	hintPercent = .4
	
	# Figure out how many letters to replace
	show = round(len(trivia["answers"][0].replace(" ",""))*hintPercent)
	if not show: show = 1

	# Pick random letters to replace
	picked = 0
	toshow = []
	replaceable = [x for x,y in enumerate(trivia["answers"][0])]
	while picked < show:
		# If it's not a space
		choice = random.choice(replaceable)
		if not trivia["answers"][0][choice] == " ":
			toshow.append(choice)
			picked += 1
			
	output = "".join([(letter if index in toshow else (" " if letter == " " else "-")) for index,letter in enumerate(trivia["answers"][0])])
	
	# If we pm it 
	if not pm:
		core.bot.say(source,"/all Hint: 02%s"%output)
	else:
		core.bot.say(source,"Hint: 02%s"%output)
	
def hintScrambledLetters(source,core,pm=0):
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if not trivia["roundstarted"]: return
	
	letters = [letter for letter in trivia["answers"][0].lower()]
	random.shuffle(letters)
	output = "Shuffled: "+" ".join(letters)
	
	# If we pm it 
	if not pm:
		core.bot.say(source,"/all Hint: 02%s"%output)
	else:
		core.bot.say(source,"Hint: 02%s"%output)
	
def giveHint(source,core,pm=0):
	hint = random.random()
	
	if hint < .8:
		hintRandomLetters(source,core,pm)
	else:
		hintScrambledLetters(source,core,pm)

def endRound(source,core,triviaEnd=0):
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]

	# Stop the timers
	for timer in trivia["timers"]: core.clock.stopTimer(timer.id)
	if trivia["nextquestiontimer"]: core.clock.stopTimer(trivia["nextquestiontimer"].id)
	
	# So we can't keep answering :)
	trivia["roundstarted"] = 0
	
	# If there's more than one round
	if trivia["rounds"] > 1 and trivia["winners"]:
		core.bot.say(source,"/all 10Current round winners: %s"%"10, ".join(["07%s - %s"%(key,trivia["winners"][key]) for key in trivia["winners"]]))

	# If it's the last round then end it
	if trivia["currentround"] == trivia["rounds"] or triviaEnd:
		if trivia["winners"]:
			roundwinner = trivia["winners"].keys()[0]
			for user in trivia["winners"]:
				if trivia["winners"][user] > trivia["winners"][roundwinner]:
					roundwinner = user
				
			# Check for ties
			winners = []
			
			# This adds the roundwinner too cause their score is equal to their score :)
			for winner in trivia["winners"]:
				if trivia["winners"][winner] == trivia["winners"][roundwinner]:
					winners.append(winner)

			if len(winners) == 1:
				core.bot.say(source,"/all 07%s10 has won this round with 07%s10 questions answered!"%(roundwinner,trivia["winners"][roundwinner]))
			else:
				core.bot.say(source,"/all 07%s10 have tied this round with 07%s10 questions answered!"%(misc.joinList(winners),trivia["winners"][roundwinner]))
		else:
			core.bot.say(source,"/all 10Nobody won this round...")
			
		stopTrivia(core,source)
	# Otherwise continue into the next round
	else:
		trivia["currentround"] += 1

		# And start another round in 10 seconds
		trivia["nextquestiontimer"] = core.clock.startTimer(10,startRound,[source,core])

def giveAnswer(source,core):
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if not trivia["roundstarted"]: return
	
	core.bot.say(source,"/all 04Time is Up! Answer: 07%s"%trivia["answers"][0])
	endRound(source,core)

def checkAnswer(core,prefix,command,arguments,network):
	source = {"user":prefix.split("!")[0],"channel":arguments[0].lower(),"network":network.name}
	
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if not trivia["roundstarted"]: return

	for answer in trivia["answers"]:
		if re.match(".*(^| |[^a-zA-Z0-9])%s($| |[^a-zA-Z0-9]).*"%answer.lower(),arguments[-1].lower()):
			maxpoints = 20
			answertime = time.time()-trivia["start"]
			score = maxpoints-round((answertime/90.0)*maxpoints)

			# Check for minumum score and if we used a hint get half score
			if score == 0: score = 1
			if source["user"].lower() in trivia["hints"]: score = round(score*.5)

			# If the user exists then update their score
			if db.goSQL("SELECT recnum FROM scores WHERE nick = '%s'",[source["user"].lower()],db="trivia"):
				db.goSQL("UPDATE scores SET score=(score+'%s') WHERE nick = '%s'",[score,source["user"].lower()],db="trivia")
			else:
				db.goSQL("INSERT INTO scores (nick, score, winningstreak, lastwin) VALUES ('%s', '%s', '1', '%s')",[source["user"].lower(),score,time.time()],db="trivia")
			info = db.goSQL("SELECT score FROM scores WHERE nick = '%s'",[source["user"].lower()],db="trivia")

			# Get the current ranking
			for ranking,name in enumerate(db.goSQL("SELECT nick FROM scores ORDER BY score DESC",db="trivia")):
				if name[0].lower() == source["user"].lower(): break

			# Save us in the winners
			if source["user"] in trivia["winners"]: trivia["winners"][source["user"]] += 1
			else: trivia["winners"][source["user"]] = 1

			# Save our streak
			if trivia["streak"][0] == source["user"].lower(): trivia["streak"][1] += 1
			else: trivia["streak"] = [source["user"].lower(),1]

			core.bot.say(source,"/all 07%s10 got the answer!! 07%s10 in 07%s10 seconds for 07%s10 points, now in 07%s10 place, Points: 07%s10, Streak: 07%s10"%(source["user"],trivia["answers"][0],round(answertime,3),score,rankText(ranking+1),info[0][0],trivia["streak"][1]))
			
			endRound(source,core)
			
			return

def askQuestion(source,core):
	if not triviaRunning(source,core): return
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if not trivia["roundstarted"]: return

	# Get a question
	triviaInfo = db.goSQL("SELECT recnum, category, question, answers FROM trivia ORDER BY RAND() LIMIT 1;",db="trivia")
	if not triviaInfo: 
		core.bot.say(source,"I have no trivia.")
		stopTrivia(core,source)
		return
		
	# Update the trivia when it was last used
	db.goSQL("UPDATE trivia SET lastuse = UNIX_TIMESTAMP() WHERE recnum = '%s'",[triviaInfo[0][0]],db="trivia")

	trivia["answers"] = triviaInfo[0][3].split("/")
	trivia["hints"] = [] # Who asked for hints.. it halves points
	trivia["start"] = time.time()

	# And start a timer for when to tell the answer and when to give a hint
	trivia["timers"] = [core.clock.startTimer(5,giveSpaces,[source,core]),
						core.clock.startTimer(30,giveHint,[source,core]),
						core.clock.startTimer(60,giveHint,[source,core]),
						core.clock.startTimer(90,giveAnswer,[source,core])]

	# And ask it
	question = triviaInfo[0][2]
	question = triviaInfo[0][1]+": "+question if triviaInfo[0][1] else question
	core.bot.say(source,"/all Trivia: 10%s. %s?"%(trivia["currentround"],question.strip("?")))

def startRound(source,core):
	core.bot.ram.get(moduleName,"trivia")[getKey(source)]["roundstarted"] = 1
	askQuestion(source,core)

def startTrivia(source,core,rounds=1):
	if triviaRunning(source,core):
		# If a user said it then tell them
		if source["user"]: core.bot.say(source,"Trivia is already running in this channel.")
		return
	else:
		resetTriviaStats(source,core)
	trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
	if trivia["roundstarted"]: return

	# Setup trivia for the right number of rounds
	trivia["rounds"] = rounds
	core.bot.say(source,"/all 10Started trivia for 07%s10 rounds!"%rounds)
	
	# And start a round
	startRound(source,core)

def handleStart(core,bot,source,split):
	rounds = int(split[0]) if split and misc.isNumber(split[0]) else 1
	if rounds == 0: rounds = 1
	if rounds > 25: bot.say(source,"Maximum rounds for trivia is 25.")
	else: startTrivia(source,core,rounds)

def handleStop(core,bot,source,split):
	if not getKey(source) in core.bot.ram.get(moduleName,"trivia"):
		bot.say(source,"Trivia is not running in this channel.")
	else:
		bot.say(source,"/all 10The game is over!!!")
		endRound(source,core,1)

def handleHint(core,bot,source,split):
	# If trivia is started and they didnt have a hint before
	if getKey(source) in core.bot.ram.get(moduleName,"trivia"):
		trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]
		
		if not source["user"].lower() in trivia["hints"] and trivia["roundstarted"]:
			trivia["hints"].append(source["user"].lower())
			giveHint(source,core,1)

def handleScores(core,bot,source,split):
	if getKey(source) in core.bot.ram.get(moduleName,"trivia"):
		trivia = core.bot.ram.get(moduleName,"trivia")[getKey(source)]

		if trivia["winners"]:
			bot.say(source,"/all 10Current round winners: %s"%"10, ".join(["07%s - %s"%(key,trivia["winners"][key]) for key in trivia["winners"]]))
		else:
			bot.say(source,"/all 10Noone is winning this round yet.")


def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network):
	if command == "privmsg": checkAnswer(core,prefix,command,arguments,network)

def setup(core):
	core.bot.ram.set(moduleName,"trivia",{})
	core.bot.ram.set(moduleName,"autotimers",[])
	
	# Start a timer for trivia in the channels of choice every 40 minutes
	source = {"user":"","channel":"#dirnt","network":"irc.freenode.net"}
	core.bot.ram.get(moduleName,"autotimers").append(core.clock.startTimer(2400,startTrivia,[source,core,1],0))
	
def cleanup(core):
	# Stop the trivia timers
	for timer in core.bot.ram.get(moduleName,"autotimers"): core.clock.stopTimer(timer.id)
	core.bot.ram.rem(moduleName,"autotimers")
	core.bot.ram.rem(moduleName,"trivia")

def addHooks(core):
	core.bot.addCommandHook("trivia",handleStart,"This makes the bot ask a trivia question.",banned=["#smc","#gameblender"])
	core.bot.addCommandHook("strivia",handleStop,"This makes the bot stop the current trivia.")
	core.bot.addCommandHook("hint",handleHint,"This makes the bot give a hint for the current question.",banned=["#smc","#gameblender"])
	core.bot.addCommandHook("tscores",handleScores,"This gives a list of the current winners and their scores.",banned=["#smc","#gameblender"])
	
def delHooks(core):
	core.bot.delCommandHook("trivia")
	core.bot.delCommandHook("strivia")
	core.bot.delCommandHook("hint")
	core.bot.delCommandHook("tscores")
