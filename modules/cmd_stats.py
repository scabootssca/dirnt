moduleName = "stats"
ramKeys = []

def handle(core,bot,source,split):
		bot.say(source,"Stats are here: http://scabootssca.dyndns.org/stats")

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("stats",handle,"This shows channel statistics.")

def delHooks(core):
	core.bot.delCommandHook("stats")
