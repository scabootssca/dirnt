moduleName = "ram"
ramKeys = []

import misc

def handle(core,bot,source,split):
	if not split:
		bot.say(source,"%s"%(misc.joinList(core.ram.storage.keys())))
	elif split[0] in core.ram.storage:
		output = str(core.ram.storage[split[0]])
		
		maxout = 300
		page = int(split[1]) if len(split) > 1 and misc.isNumber(split[1]) else 1
		
		bot.say(source,"%s[%s/%s] = %s"%(split[0],page,(len(output)/maxout)+1,output[maxout*(page-1):maxout*(page)]))

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("ram",handle,"This displays info about the bots ram.",30)

def delHooks(core):
	core.bot.delCommandHook("ram")
