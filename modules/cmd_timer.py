moduleName = "timer"
ramKeys = []

import misc,time

def handleTimerinfo(core,bot,source,split):
	for key,value in zip(core.ram.get("clock","timers").keys(),core.ram.get("clock","timers").values()):
		bot.say(source,"%s. (%s long) (%s remaining) (repeat %s time(s)) %s"%(key,misc.descriptiveTimeDelta(value.length,0),misc.descriptiveTimeDelta(time.time(),value.start),value.repeat if value.repeat else "unlimited",str(value.function)))

def handleStopTimer(core,bot,source,split):
	if split:
		if split[0] in core.ram.get("clock","timers").keys():
			core.clock.stopTimer(split[0])
			bot.say(source,"Timer #%s stopped.",split[0])
		else:
			bot.say(source,"Timer #%s does not exist.",split[0])
	else:
		bot.say(source,"Please specify a timer.")

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("timerinfo",handleTimerinfo,"This displays info about running timers.",30)
	core.bot.addCommandHook("stoptimer",handleStopTimer,"This stops a running timer.",30)

def delHooks(core):
	core.bot.delCommandHook("timerinfo")
 	core.bot.delCommandHook("stoptimer")
