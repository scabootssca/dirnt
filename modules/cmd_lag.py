moduleName = "lag"
ramKeys = ["lag","timer"]

import re, misc, time

def lagPing(core):
	for network in core.client.networks.values():
		network.send("PING %s lag:%s"%(network.name,time.time()))

def handleLag(core,bot,source,split):
	if source["network"] in core.bot.ram.get(moduleName,"lag"):
		lagPing(core)
		bot.say(source,"Lag is %s seconds."%round(core.bot.ram.get(moduleName,"lag")[source["network"]],2))
	else:
		lagPing(core)
		bot.say(source,"Lag is unknown.")

def handleLagping(core,bot,source,split):
	lagPing(core)
	bot.say(source,"Pinged..")

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network):
	# If the network pongs us
	if command == "pong":
		if arguments[1] == network.name:
			if re.match("^lag\:[0-9]+\.?[0-9]*",arguments[0]):
				if misc.isNumber(arguments[0].split(":")[1]):
					core.bot.ram.get(moduleName,"lag")[network.name] = time.time()-float(arguments[0].split(":")[1])
	
def setup(core):
	core.bot.ram.set(moduleName,"lag",{})
	
	# Every 3 minutes ping the server for lag, and one at start for good measure
	lagPing(core)
	core.bot.ram.set(moduleName,"timer",core.clock.startTimer(180,lagPing,[core],0))
	
def cleanup(core):
	core.clock.stopTimer(core.bot.ram.get(moduleName,"timer").id)
	
	core.bot.ram.rem(moduleName,"lag")
	core.bot.ram.rem(moduleName,"timer")

def addHooks(core):
	core.bot.addCommandHook("lag",handleLag,"This tells how much the bot is lagging.")
	core.bot.addCommandHook("lagping",handleLagping,"This makes the bot send a lag ping.")

def delHooks(core):
	core.bot.delCommandHook("lag")
	core.bot.delCommandHook("lagping")

