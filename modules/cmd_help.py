moduleName = "help"
ramKeys = []

def handle(core,bot,source,split):
	commandList = [command for command in core.ram.get("bot","commands") if bot.hasAccess(source["prefix"],core.ram.get("bot","commands")[command][2])]
	bot.say(source,"My commands are: %s"%", ".join(commandList))

def handleNetout(core,dest,message,network): pass
def handleNetin(core,prefix,command,arguments,network): pass
def setup(core): pass
def cleanup(core): pass

def addHooks(core):
	core.bot.addCommandHook("help",handle,"This lists help commands for the bot.")
	
def delHooks(core):
	core.bot.delCommandHook("help")
