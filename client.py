import socket, select, time, re, netin, settings, db, thread

lineSplit = re.compile("\r?\n")
messageSplit = re.compile("^(:(?P<prefix>[^ ]+) +)?(?P<command>[^ ]+)( *(?P<argument> .+))?")

class Channel:
	def __init__(self,name):
		self.name = name
		self.users = []
	
	def addUser(self,user):
		if not user in self.users:
			self.users.append(user)
			
	def removeUser(self,user):
		if user in self.users:
			self.users.remove(user)

class Network:
	def __init__(self,core,name):
		self.core = core
		self.name = name
		
		self.socket = 0
		self.buffer = ""
		self.connected = 0

	def setup(self,name,servers,port,nick,user,password):
		info = {}
		info["name"] = name
		info["servers"] = servers
		info["port"] = port
		info["nick"] = nick
		info["user"] = user
		info["password"] = password

		info["channels"] = {}

		self.core.ram.set(self.name,"networkInfo",info)
	
	def cleanup(self):
		self.disconnect()
		self.core.ram.rem(self.name,"networkInfo")
		
	def set(self,name,value):
		self.core.ram.get(self.name,"networkInfo")[name] = value
	
	def get(self,name):
		return self.core.ram.get(self.name,"networkInfo")[name]
		
	def joinChannel(self,name):
		""" This makes the bot join a channel """
		if not name in self.get("channels"):
			self.get("channels")[name] = Channel(name)
			self.send("JOIN %s"%name)
		
	def partChannel(self,name,message=""):
		""" This makes the bot leave a channel """
		if name in self.get("channels"):
			del self.get("channels")[name]
			self.send("PART %s :%s"%(name,message))
		
	def sendConnectionInfo(self):
		""" This is called when the bot connects and sends the bots connection info and joins channels """
		self.send("NICK %s"%self.get("nick"))
		self.send("PASS %s"%self.get("password"))
		self.send("USER %s 0 * :%s"%(self.get("nick"),self.get("user")))
		
		# Join startup channels
		for channel in self.get("channels"):
			self.send("JOIN %s"%channel)

	def connect(self):
		""" This connects to a host/port """
		# Make a socket 
		self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

		# Try to connect
		try:
			self.socket.connect((self.get("servers")[0], self.get("port")))
			self.connected = 1
		except:
			self.disconnect("Connection reset by peer. (Error connecting)")
			self.reconnect()

		self.sendConnectionInfo()

	def disconnect(self,reason=""):
		""" This disconnects the socket """
		if not self.connected: return
		
		self.core.ui.output("Disconnected.... %s"%reason)
		self.socket.close()
		self.connected = 0
		self.socket = 0
		
	def reconnect(self):
		self.core.ui.output("Reconnecting in %s seconds."%settings.reconnectSleep)
		time.sleep(settings.reconnectSleep)
		self.connect()
		
	def send(self,data):
		""" This sends to the socket """
		if not self.connected: return
		
		# Try to send it
		try:
			# Log the line
			db.logSocket("out",data)
			
			self.socket.send(data+"\r\n")
		except socket.error, x:
			self.disconnect("Connection reset by peer. (Error sending)")
			self.reconnect()
		
	def recv(self):
		""" This recieves from the socket """
		if not self.connected: return
		data = ""

		# Try to recieve from the socket
		try:
			data = self.socket.recv(4096)
		except socket.error, x:
			self.disconnect("Connection reset by peer. (Error recieving)")
			self.reconnect()
			return
			
		# If we recieved nothing we got disconnected
		if not data:
			self.disconnect("Connecting reset by peer. (Nothing recieved)")
			self.reconnect()
			return
		
		# Split the message into lines
		lines = lineSplit.split(self.buffer + data)

		# Save the last, unfinished line.
		self.buffer = lines[-1]
		lines = lines[:-1]

		# Handle the lines
		for line in lines:
			if not line: continue

			# Log the line
			db.logSocket("in",line)

			prefix = None
			command = None
			arguments = None

			m = messageSplit.match(line)

			if m.group("prefix"):
				prefix = m.group("prefix")

			if m.group("command"):
				command = m.group("command").lower()

			if m.group("argument"):
				a = m.group("argument").split(" :", 1)
				arguments = a[0].split()
				if len(a) == 2: arguments.append(a[1])
				
			# Handle it
			netin.handle(self.core,prefix,command,arguments,self)

class Client:
	def __init__(self,core):
		self.core = core
		self.networks = {}
		self.running = 1

	def setup(self):
		""" This is run to setup the variables. """
		pass
		
	def cleanup(self):
		# Disconnect from all networks and stop the recieve loop
		self.running = 0
		self.disconnect("",1)

	def joinChannel(self,name,channel):
		""" Makes the specified network join a channel. """
		if name in self.networks:
			self.networks[name].joinChannel(channel)

	def partChannel(self,name,channel):
		""" Makes the specified network part a channel. """
		if name in self.networks:
			self.networks[name].partnChannel(channel)

	def connect(self,name,servers,port,nick,user,password):
		""" Connects to the specified network """
		network = Network(self.core,name)
		network.setup(name,servers,port,nick,user,password)
		network.connect()
		
		self.networks[name] = network
		return network
		
	def disconnect(self,name="",cleanup=0):
		""" Disconnects to the specified network. """
		if not name: names = self.networks.keys()
		elif name in self.networks: names = [name]
		else: return
		
		for name in names[:]:
			if cleanup: self.networks[name].cleanup()
			else: self.networks[name].disconnect()
			del self.networks[name]
	
	def reconnect(self,name=""):
		""" Reconnects to the specified network. """
		if not name: names = self.networks.keys()
		elif name in self.networks: names = [name]
		else: return
		
		for name in names[:]:
			self.networks[name].reconnect()

	def send(self,name,data):
		""" Sends 'data' to 'network' if we're connected. """
		if name in self.networks:
			self.networks[name].send(data)

	def recvLoop(self,timeout=0.3):
		""" Starts a recieve loop to recieve from the networks. """
		while self.running:
			# Get the sockets of any connected network
			sockets = {}
			for network in self.networks.values():
				if network.connected: sockets[network.socket] = network
				
			# If we're connected to a network recieve from everything that has stuff waiting
			if sockets:
				for socket in select.select(sockets.keys(), [], [], timeout)[0]:
					if self.running: sockets[socket].recv()
					else: return
			else:
				time.sleep(timeout)
				
	def mainLoop(self,timeout=0.3):
		thread.start_new_thread(self.recvLoop,(timeout,))
