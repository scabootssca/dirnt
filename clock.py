import ram, time, threading

class Timer:
	def __init__(self,clock,id,length,function,args,repeat):
		self.clock = clock
		self.id = id
		
		self.length = length
		self.function = function
		self.args = args
		self.repeat = repeat
		
		self.running = 1
		self.iteration = 0
		self.start = time.time()
		
		self.thread = threading.Timer(self.length,self.iterate)
		self.thread.start()
	
	def stop(self):
		self.running = 0
		self.thread.cancel()
		
	def iterate(self):
		self.iteration += 1
		
		# See if it's still running
		if not self.running: return

		# Call the function
		self.function(*tuple(self.args))
		
		# If this was the last iteration, cleanup and end
		if self.iteration == self.repeat:
			self.clock.stopTimer(self.id)
			return

		# Start a new iteration and set a new start time
		self.start = time.time()
		self.thread = threading.Timer(self.length,self.iterate)
		self.thread.start()

class Clock:
	def __init__(self,core):
		self.core = core

	def setup(self):
		self.core.ram.set("clock","timers",{}) 
		self.core.ram.set("clock","timerid",0)
		self.core.ram.set("clock","birth",time.time())
		
	def cleanup(self):
		# Stop all the timers
		for timer in self.core.ram.get("clock","timers").values(): timer.stop()
		self.core.ram.rem("clock","timers") 
		self.core.ram.rem("clock","timerid")
		self.core.ram.rem("clock","birth")


	def startTimer(self,length,function,args=[],repeat=1):
		timer = Timer(self,self.core.ram.get("clock","timerid"),length,function,args,repeat)
		self.core.ram.get("clock","timers")[timer.id] = timer
		self.core.ram.set("clock","timerid",timer.id+1)
		return timer
		
	def stopTimer(self,id):
		if id in self.core.ram.get("clock","timers"):
			self.core.ram.get("clock","timers")[id].stop()
			del self.core.ram.get("clock","timers")[id]
