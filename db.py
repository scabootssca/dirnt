import time, ui
import MySQLdb as sql

class SQLQuery:
	def __init__(self,db="dirnt",user="dirnt",password="master",host="localhost"):
		while 1:
			try:
				# Connect
				self.connection = sql.connect(host=host,user=user,passwd=password,db=db)
				self.cursor = self.connection.cursor()
				return
			except:
				print "ERROR <user ('%s') cannot connect to database ('%s'), waiting 5 seconds.>"%(user,db)
				time.sleep(5)

	def execute(self,query,arguments=[]):
		# Loop through the arguments and make it sane
		arguments = map(lambda x: self.connection.escape_string(str(x)),arguments)

		# Execute it and get the result
		self.cursor.execute(query%tuple(arguments))
		result = self.cursor.fetchall()
		
		# Update the database
		self.connection.commit()
		
		return result
		
	def close(self):
		self.cursor.close()
		self.connection.close()
		
def goSQL(statement,values=[],db="dirnt",user="dirnt",password="master",host="localhost"):
	# Perform the query
	query = SQLQuery(db,user,password,host)
	result = query.execute(statement,values)
	query.close()

	# Return the result
	return result
	
	#while 1:
		#try:
			## Connect
			#connection = sql.connect(host=host,user=user,passwd=password,db=db)
			#break
		#except:
			#print "ERROR <cannot connect to database, waiting 5 seconds.>"
			#time.sleep(5)
		
	#cursor = connection.cursor()
	
	## Loop through the arguments and make it sane
	#values = map(lambda x: connection.escape_string(str(x)),values)

	## Execute it and get the result
	#cursor.execute(statement%tuple(values))
	#result = cursor.fetchall()
	#cursor.close()
	
	## Update the database and close the connection
	#connection.commit()
	#connection.close()
		

def logConversation(command,message,source,channel,network):
	goSQL("INSERT INTO convolog (timestamp, type, message, nick, channel, network) VALUES (NOW(), '%s', '%s', '%s', '%s', '%s');",[command,message,source,channel,network])

def logSocket(type,message):
	goSQL("INSERT INTO socketlog (timestamp, type, message) VALUES (NOW(), '%s', '%s');",[type,message])

def logError(type,message):
	goSQL("INSERT INTO errorlog (timestamp, type, message) VALUES (NOW(), '%s', '%s');",[type,message])
