import time, db, sys

class StdRedirect:
	def __init__(self,prefix,func):
		self.prefix = prefix
		self.func = func
		
	def write(self,output):
		self.func(prefix+output)

class Ui:
	def __init__(self,core):
		self.core = core
		#self.oldStd = [sys.stdout,sys.stderr]

	def setup(self):
		#sys.stdout = StdRedirect("StdOut: ",self.output)
		#sys.stderr = StdRedirect("StdErr: ",self.error)
		pass
		
	def cleanup(self):
		#sys.stdout = self.oldStd[0]
		#sys.stderr = self.oldStd[1]
		pass
	
	# Connection to core
	def joinNetwork(self): pass
	def partNetwork(self): pass
	def joinChannel(self): pass
	def partChannel(self): pass
	
	def updateUsers(self): pass

	def write(self,type,message,source,channel="",network="",networkMsg=0):
		# If it's a dict then set everything from that
		if not hasattr(source,"split"):
			network = source["network"]
			channel = source["channel"]
			source = source["user"]

		print "%s %s:%s <%s> %s"%(time.strftime("[%H:%M:%S]"),type,channel,source,message)
		db.goSQL("INSERT INTO consolelog (timestamp, message) VALUES (NOW(), '%s')",["%s %s:%s <%s> %s"%(time.strftime("[%H:%M:%S]"),type,channel,source,message)])
		
	def error(self,message):
		print "%s ERROR: %s"%(time.strftime("[%H:%M:%S]"),message)
		db.goSQL("INSERT INTO errorlog (timestamp, message, type) VALUES (NOW(), '%s', 'console')",[message])

	def output(self,message):
		print "%s %s"%(time.strftime("[%H:%M:%S]"),message)
		db.goSQL("INSERT INTO consolelog (timestamp, message) VALUES (NOW(), '%s')",[message])
		

