#!/usr/bin/python
# ___________________________
#/ Irc Bot                   \
#| By: scabootssca           |
#| At: scabootssca@gmail.com |
#| On: 21.June.2008          |
#\___________________________/
#
import client, bot, ram, ui, clock, time

class Dirnt:
	def __init__(self):
		self.running = 1

		# Initilize the modules
		self.ram    = ram.Ram(self)
		self.clock  = clock.Clock(self)
		self.client = client.Client(self)
		self.bot    = bot.Bot(self)
		self.ui     = ui.Ui(self)

		# And set them up
		self.ram.setup()
		self.ui.setup()
		self.bot.setup()
		self.client.setup()
		self.clock.setup()

	def stop(self):
		self.running = 0

		# Unload all modules in reverse order
		self.ui.cleanup()
		self.bot.cleanup()
		self.client.cleanup()
		self.clock.cleanup()
		self.ram.cleanup()

	def mainLoop(self):
		while self.running:
			time.sleep(1)
			pass


dirnt = Dirnt()

dirnt.bot.loadAllModules()

dirnt.client.connect("irc.freenode.net",["irc.freenode.net"],6667,"dirnt","Archibald Gaylord","")
dirnt.client.joinChannel("irc.freenode.net","#smc")
dirnt.client.joinChannel("irc.freenode.net","#dirnt")
dirnt.client.joinChannel("irc.freenode.net","#gameblender")
dirnt.client.joinChannel("irc.freenode.net","#pscab")
dirnt.client.mainLoop()


dirnt.mainLoop()

# [X] Need to make reload a lil better... need to store the cleanup and setup functions even after reloading or something...
# Need to make the ram clearable
# [X] Need to make it check if any ram stuff is not in the new loading and delete old stuff
# [ ] Need to make the modules more robust and have every entity that has ram inhereted from ram stuffs so we can get all their info
# [ ] Need to make modules know if they are unloaded or not
