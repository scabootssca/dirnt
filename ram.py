class Ram:
	def __init__(self,core):
		self.core = core
		
		self.storage = {}
		
	def setup(self): pass
	def cleanup(self):
		self.storage = {}
	
	def set(self,module,key,value):
		""" This stores something in the bots ram """
		self.storage["%s_%s"%(module,key)] = value
		
	def get(self,module,key):
		""" This loads something from the bots ram """
		return self.storage["%s_%s"%(module,key)]
	
	def has(self,module,key):
		""" This returns if something is in the bots ram """
		return "%s_%s"%(module,key) in self.storage
			
	def rem(self,module,key):
		""" This deletes something from the bots ram """
		if "%s_%s"%(module,key) in self.storage:
			del self.storage["%s_%s"%(module,key)]
		
	def purge(self,module):
		""" This deletes everything stored by a module """
		for key in self.storage.keys()[:]:
			if key.startswith("%s_"%module):
				del self.storage[key]
