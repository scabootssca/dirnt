import settings, traceback, db

def hMsg(core,prefix,command,arguments,network):
	if not prefix: prefix = "*!*@*"
	if not '@' in prefix:
		nick = prefix
		mask = ''
	else:
		nick = prefix.split("!")[0]
		mask = prefix.split("@")[1]
	
	message = arguments[-1]
	
	# If it's an action then display it right
	if message.startswith("\x01ACTION"):
		message = " ".join(message.split()[1:]).strip("\x01")
		command = "action"
	# They asked my version so I send them my answer back
	if message.startswith("\x01VERSION"):
		network.send("NOTICE %s :\001VERSION %s\001"%(nick,settings.version))
		return
	# They send me a ping so I send it back
	if message.startswith("\x01PING"):
		network.send("NOTICE %s :\001PING %s\001"%(nick," ".join(message.split()[1:]).strip("\x01")))
		return

	# Get the source
	source = {"user":nick,"channel":arguments[0].lower(),"network":network.name}

	# If it's from my name then it is a private message
	#if source["channel"] == network.userInfo["nick"]:
	#	source["channel"] = source["user"]

	# If it's from a new channel and it's a pm then join it and add the user to it
	#if not source["channel"] in network.channels:
	#	# If it's from a channel join it otherwise drop it
	#	if not source["channel"].startswith("#"):
	#		network.joinChannel(source["channel"],0)
	#		network.channels[source["channel"]].addUser(nick)
	#	else:
	#		return
		
	# If it's from a user we missed, then add them
	#if not nick in network.channels[source["channel"]].users: network.channels[source["channel"]].addUser(nick,mask)

	# Get the user
	#user = network.channels[source["channel"]].users[nick]

	# Set the users mask if it is not set
	#if not user.mask: user.mask = mask

	# Add the user if hes not here
	for channel in network.get("channels").values():
		if channel.name == source["channel"]:
			channel.addUser(source["user"])

	# Tell the ui
	core.ui.write(command,message,source)

	# If it's a private message, tell the bot
	usedBot = 0
	if command == "privmsg" and message.strip():
		usedBot = core.bot.process(prefix,source,message.split())

	# Log it
	if not usedBot:
		db.logConversation(command,message,source["user"],source["channel"],source["network"])

def hPing(core,prefix,command,arguments,network):
	network.send("PONG "+arguments[0])

def hJoin(core,prefix,command,arguments,network):
	nick = prefix.split("!")[0]
	mask = prefix.split("@")[1]

	source = {"user":nick,"channel":arguments[0].lower(),"network":network.name}
	
	network.get("channels")[source["channel"]].addUser(nick)
	
	core.ui.write(command,"%s(%s) has joined %s"%(nick,mask,arguments[0]),source)
	db.logConversation(command,"",source["user"],source["channel"],source["network"])
	
def hPart(core,prefix,command,arguments,network):
	nick = prefix.split("!")[0]
	mask = prefix.split("@")[1]

	source = {"user":nick,"channel":arguments[0].lower(),"network":network.name}

	network.get("channels")[source["channel"]].removeUser(nick)

	core.ui.write(command,"%s(%s) has left %s (%s)"%(nick,mask,arguments[0],arguments[1]),source)
	db.logConversation(command,arguments[1],source["user"],source["channel"],source["network"])
	
def hQuit(core,prefix,command,arguments,network):
	nick = prefix.split("!")[0]
	mask = prefix.split("@")[1]
	
	for channel in network.get("channels").values():
		if nick in channel.users:
			source = {"user":nick,"channel":channel.name,"network":network.name}

			channel.removeUser(nick)
			
			core.ui.write(command,"%s(%s) has quit (%s)"%(nick,mask,arguments[0]),source)
			db.logConversation(command,arguments[0],source["user"],source["channel"],source["network"])

def hNick(core,prefix,command,arguments,network):
	nick = prefix.split("!")[0]
	mask = prefix.split("@")[1]

	for channel in network.get("channels").values():
		if nick in channel.users:
			source = {"user":nick,"channel":channel.name,"network":network.name}

			channel.addUser(arguments[0])
			channel.removeUser(nick)

			core.ui.write(command,"%s(%s) is now known as '%s'"%(nick,mask,arguments[0]),source)
			db.logConversation(command,arguments[0],source["user"],source["channel"],source["network"])

lookup = {"privmsg":hMsg,
		  "notice":hMsg,
		  "ping":hPing,
		  "join":hJoin,
		  "part":hPart,
		  "quit":hQuit,
		  "nick":hNick}

def handle(core,prefix,command,arguments,network):		  
	""" This handles incoming messages """
	try:
		if command in lookup: lookup[command](core,prefix,command,arguments,network)
		else: core.ui.write(command,str(arguments),"","",network.name,1)
		for module in core.ram.get("bot","modules").values():
			module.handleNetin(core,prefix,command,arguments,network)
	except:
		traceback.print_exc()
