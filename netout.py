import traceback

def hSay(core,dest,message,network):
	network.send("PRIVMSG %s :%s"%(dest["channel"],message))

def hNotice(core,dest,message,network):

	split = message.split()
	dest["user"] = split.pop(0)
	message = " ".join(split)

	network.send("NOTICE %s :%s"%(dest["user"],message))
	
def hQuit(core,dest,message,network):
	network.send("QUIT :%s"%message)
	network.disconnect()

def hMe(core,dest,message,network):
	network.send("PRIVMSG %s :\x01ACTION %s\x01"%(dest["channel"],message))

def hJoin(core,dest,message,network):
	network.joinChannel(message.split()[0])
	
def hPart(core,dest,message,network):
	split = message.split()
	network.partChannel(split[0]," ".join(split[1:]))

lookup = {"say":hSay,
		  "notice":hNotice,
		  "quit":hQuit,
		  "me":hMe,
		  "join":hJoin,
		  "part":hPart}

def handle(core,dest,message):
	""" This handles outgoing messages """
	try:
		core.ui.write("sending",message,dest,networkMsg=1)
		
		if not dest["network"] in core.client.networks: return
		if not message: return
		split = message.split()
		
		# If it's a command
		if split[0].startswith("/"):  type = split.pop(0)[1:]
		else: type = "say"

		message = " ".join(split)

		network = core.client.networks[dest["network"]]
		if type in lookup: lookup[type](core,dest,message,network)
		for module in core.ram.get("bot","modules").values():
			module.handleNetout(core,dest,message,network)
	except:
		traceback.print_exc()
