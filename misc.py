def joinList(list):
	""" This joins a list using this format 'a, b and c' """
	list = [str(x) for x in list]

	if not list: return ""
	if len(list) == 1: return list[0]
	return " and ".join([", ".join(list[:-1]),list[-1]])

def descriptiveTimeDelta(now,then,verbose=1):
	ago1 = int(round(now-then))

	mintime = 60
	hrtime = mintime*60
	daytime = hrtime*24
	weektime = daytime*7

	# How many weeks ago
	ago2 = ago1%weektime
	weeks = int((ago1-ago2)/weektime)

	# How many days ago
	ago1 = ago2%daytime
	days = int((ago2-ago1)/daytime)

	# How many hours ago
	ago2 = ago1%hrtime
	hours = int((ago1-ago2)/hrtime)

	# How many minutes ago
	ago1 = ago2%mintime
	minutes = int((ago2-ago1)/mintime)

	# How many seconds ago
	seconds = ago1

	if verbose:
			ago = []
			if weeks: ago.append("%s week%s"%(weeks,"s" if weeks != 1 else ""))
			if days: ago.append("%s day%s"%(days,"s" if days != 1 else ""))
			if hours: ago.append("%s hour%s"%(hours,"s" if hours != 1 else ""))
			if minutes: ago.append("%s minute%s"%(minutes,"s" if minutes != 1 else ""))
			if seconds: ago.append("%s second%s"%(seconds,"s" if seconds != 1 else ""))
			if len(ago) > 1: ago = " and ".join([", ".join(ago[:-1]),ago[-1]])
			else: ago = ago[0]
			return ago
			
	return weeks,days,hours,minutes,seconds


def isNumber(test):
	try:
		float(test)
		return 1
	except:
		return 0
