import os, re, sys, thread, netout, traceback, settings, ram

class Bot:
	def __init__(self,core):
		self.core = core
		
		# Get the bots ram from ram if it exists
		if self.core.ram.has("bot","moduleRam"):
			self.ram = self.core.ram.get("bot","moduleRam")
		
	def setup(self):
		self.core.ram.set("bot","modules",{}) # {name:module}
		self.core.ram.set("bot","commands",{}) # {name:function}
		
		# Setup bots ram
		self.ram = ram.Ram(self.core)
		self.ram.setup()
		self.core.ram.set("bot","moduleRam",self.ram)

	def cleanup(self):
		# Unload all modules
		for module in self.core.ram.get("bot","modules").keys()[:]: self.unloadModule(module)
		self.core.ram.rem("bot","modules")
		self.core.ram.rem("bot","commands")
		self.core.ram.rem("bot","moduleRam")

########################
## Module Stuff
	def loadModule(self,module):
		""" Loads all the functions from a module into the bot """
		
		print "Loading module (%s)"%(module.moduleName).ljust(50,".")
		try:
			reload(module)

			if not module.moduleName in self.core.ram.get("bot","modules"):
				self.core.ram.get("bot","modules")[module.moduleName] = module
				module.setup(self.core)	
				module.addHooks(self.core)
		except:
			self.core.ui.error("Error loading module: %s"%module)
			traceback.print_exc()
			traceback.print_stack()

	def reloadModule(self,moduleName):
		""" Reloads a loaded module but does not re initilize it """
		# Make it belong to the bot
		#moduleName = "bot_"+moduleName
		
		if moduleName in self.core.ram.get("bot","modules"):
			try:
				# Cleanup the old one
				oldModule = self.core.ram.get("bot","modules")[moduleName]
				oldModule.delHooks(self.core)

				# Get the new info
				module = reload(oldModule)
	
				# Remove the ram keys that are not in the new one
				for ramKey in [ramKey for ramKey in oldModule.ramKeys if not ramKey in module.ramKeys]:
					self.ram.rem(oldModule.moduleName,ramKey)
				
				# Delete the old one
				del self.core.ram.get("bot","modules")[moduleName]
				
				# Load it again
				self.core.ram.get("bot","modules")[module.moduleName] = module
				module.addHooks(self.core)
				
				return 1
			except:
				self.core.ui.error("Error reloading module: %s"%moduleName)
				traceback.print_exc()
				traceback.print_stack()
				
		return 0
			
	def unloadModule(self,moduleName):
		""" Unloads a loaded module """
		# Make it belong to the bot
		#moduleName = "bot_"+moduleName
		
		print "Unloading module (%s)".ljust(50,".")
		if moduleName in self.core.ram.get("bot","modules"):
			try:
				module = self.core.ram.get("bot","modules")[moduleName]

				module.cleanup(self.core)
				module.delHooks(self.core)
				
				# Remove everything from the bots ram that we might of missed
				self.ram.purge(module.moduleName)
				
				del self.core.ram.get("bot","modules")[moduleName]
				print "[Success!]"
				return 1
			except:
				print "[Fail....]"
				self.core.ui.error("Error unloading module: %s"%moduleName)
				traceback.print_exc()
				
		return 0
			
	def resetModule(self,moduleName):
		""" Reloads a modules and reinitilizes it """
		if moduleName in self.core.ram.get("bot","modules"):
			module = self.core.ram.get("bot","modules")[moduleName]
			# Try to unload and reload it and return true if there were no errors
			if self.unloadModule(moduleName) and self.loadModule(module):
				return 1
			
		return 0

	def addCommandHook(self,name,command,help="",level=0,banned=[],banmsg=""):
		""" Adds a hook for a command named name to the bot """
		if not name in self.core.ram.get("bot","commands"):
			self.core.ram.get("bot","commands")[name] = [command,help,level,banned,banmsg]
			
	def delCommandHook(self,name):
		""" Deletes a hook for a command from the bot """
		if name in self.core.ram.get("bot","commands"):
			del self.core.ram.get("bot","commands")[name]

	def loadAllModules(self):
		""" Looks in the module_dir folder for modules """
		# Load all the modules in the modules/
		sys.path.append("modules")
		error = 0
		for moduleName in [module for module in os.listdir("modules/") if re.match("^cmd_\w+\.py$",module)]:
			try:
				module = __import__(moduleName[:-3])
				self.loadModule(module)
			except:
				self.core.ui.error("Error importing module: %s"%moduleName)
				traceback.print_exc()
				traceback.print_stack()
				error = 1
	
		return error
	
	def unloadAllModules(self):
		""" Unloads every module that's loaded """
		error = 0
		for moduleName in self.core.ram.get("bot","modules").keys()[:]:
			if not self.unloadModule(moduleName): error = 1
		return error

	def reloadAllModules(self):
		""" Reloads every module that's loaded but does not unset variables """
		error = 0
		for moduleName in self.core.ram.get("bot","modules").keys()[:]:
			if not self.reloadModule(moduleName): error = 1
		return error

	def resetAllModules(self):
		""" Reloads every module that's loaded and resets them """
		error = 0
		for moduleName in self.core.ram.get("bot","modules").keys()[:]:
			if not self.resetModule(moduleName): error = 1
		return error

########################
# Bot Stuff
	def hasAccess(self,prefix,level):
		if not level: return 1
		for user in settings.accessList:
			if re.compile(user).match(prefix) and settings.accessList[user] >= level:
				return 1

	def say(self,dest,message):
		""" This says 'message' to 'dest' """
		if not message.startswith("/"):
			message = "/notice %s %s"%(dest["user"],message)
		elif message.split()[0] == "/all": message = " ".join(message.split()[1:])
		elif message.split()[0] == "/pm":
			message = " ".join(message.split()[1:])
			dest["channel"] = dest["user"]
		
		netout.handle(self.core,dest,message)

	def process(self,prefix,source,split):
		""" This processes an incoming message and sends it to the appropriate bot commands """
		command = split.pop(0)
		if command.startswith("-"):
			command = command[1:]
			if command in self.core.ram.get("bot","commands"):
				info = self.core.ram.get("bot","commands")[command]
				# Check if we have access
				if self.hasAccess(prefix,info[2]):
					# Check if it's not banned
					if not source["channel"].lower() in info[3]:
						source["prefix"] = prefix
						thread.start_new_thread(self.runCommand,(command,info[0],source,split))
					else:
						if info[4]: self.say(source,"This command is banned here: %s"%info[4])
						else: self.say(source,"This command is banned in this channel.")
				else:
					self.say(source,"Nope")
				return 1
					
	def runCommand(self,name,command,source,split):
		""" This is just a function for running a command in the same thread as it so we can catch errors """
		try:
			command(self.core,self,source,split)
		except:
			self.core.ui.error("Error running command: %s, [%s], [%s]"%(name,", ".join(split),", ".join(source.values())))
			print "FUCKSHITERROR"
			traceback.print_exc()
			traceback.print_stack()
