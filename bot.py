import os, re, sys, thread, netout, traceback, settings, ram

class Bot:
	def __init__(self,core):
		self.core = core
		
		# Get the bots ram from ram if it exists
		if self.core.ram.has("bot","moduleRam"):
			self.ram = self.core.ram.get("bot","moduleRam")
		
	def setup(self):
		self.core.ram.set("bot","modules",{}) # {name:module}
		self.core.ram.set("bot","commands",{}) # {name:function}
		
		# Setup bots ram
		self.ram = ram.Ram(self.core)
		self.ram.setup()
		self.core.ram.set("bot","moduleRam",self.ram)

	def cleanup(self):
		# Unload all modules
		for module in self.core.ram.get("bot","modules").keys()[:]: self.unloadModule(module)
		self.core.ram.rem("bot","modules")
		self.core.ram.rem("bot","commands")
		self.core.ram.rem("bot","moduleRam")

	def reportError(self,text):
		self.core.ui.error(text)
		#traceback.print_exc()
		#traceback.print_stack()

########################
## Module Stuff
	def loadModule(self,moduleName):
		""" Loads all the functions from a module into the bot """
		
		print ("Loading module (%s)"%moduleName).ljust(50,"."),
		try:
			# Import and reload the module
			module = __import__(moduleName)
			reload(module)
			
			# If it exists then add it otherwise fail
			if not module.moduleName in self.core.ram.get("bot","modules"):
				
				# Add the module into the bots module list
				self.core.ram.get("bot","modules")[module.moduleName] = module
				
				# Load the module
				module.setup(self.core)	
				module.addHooks(self.core)
				
				print "[Success!]"
				return 1
			else:
				print "[Fail....] Module Exists Already"
		except:
			print "[Fail....]"
			self.reportError("Error loading module: %s"%module)

			
	def reloadModule(self,moduleName):
		""" Reloads a loaded module but does not reset any stored ram keys """

		# If it does not exist then return
		if not moduleName in self.core.ram.get("bot","modules"): return
		
		# Try reloading it
		print ("Reloading module (%s)"%moduleName).ljust(50,"."),
		try:
			# Delete the old bot hooks
			module = self.core.ram.get("bot","modules")[moduleName]
			module.delHooks(self.core)

			# Reparse the module fule
			newModule = reload(module)
	
			# Remove the ram keys that are not in the new one
			for ramKey in [ramKey for ramKey in module.ramKeys if not ramKey in newModule.ramKeys]:
				self.ram.rem(module.moduleName,ramKey)
				
			# Update the bots module database and add the new hooks
			self.core.ram.get("bot","modules")[module.moduleName] = newModule
			newModule.addHooks(self.core)
				
			print "[Success!]"
			return 1
		except:
			print "[Fail....]"
			self.reportError("Error reloading module: %s"%moduleName)

	def unloadModule(self,moduleName):
		""" Unloads a loaded module """
		
		# If the module does not exist then return
		if not moduleName in self.core.ram.get("bot","modules"): return
		
		print ("Unloading module (%s)"%moduleName).ljust(50,"."),
		try:
			# Get the module
			module = self.core.ram.get("bot","modules")[moduleName]

			# Call the modules cleanup function and remove its hooks
			module.cleanup(self.core)
			module.delHooks(self.core)
			
			# Remove everything from the bots ram that we might of missed
			self.ram.purge(module.moduleName)
			
			# Remove the module from the bots module database
			del self.core.ram.get("bot","modules")[moduleName]
			
			print "[Success!]"
			return 1
		except:
			print "[Fail....]"
			self.reportError("Error unloading module: %s"%moduleName)
			
	def resetModule(self,moduleName):
		""" Unloads a module then reloads it """
		
		# If the module does not exist then return
		if not moduleName in self.core.ram.get("bot","modules"): return
		
		print "Resetting module (%s)".ljust(50,"."),
		if self.unloadModule(moduleName) and self.loadModule(moduleName):
			print "[Success!]"
			return 1
		else:
			print "[Fail....]"

	def addCommandHook(self,name,command,help="",level=0,banned=[],banmsg=""):
		""" Adds a hook for a command named name to the bot """
		if not name in self.core.ram.get("bot","commands"):
			self.core.ram.get("bot","commands")[name] = [command,help,level,banned,banmsg]
			
	def delCommandHook(self,name):
		""" Deletes a hook for a command from the bot """
		if name in self.core.ram.get("bot","commands"):
			del self.core.ram.get("bot","commands")[name]

	def loadAllModules(self):
		""" Looks in the module_dir folder for modules """
		# Load all the modules in the modules/
		sys.path.append("modules")

		for moduleName in [module for module in os.listdir("modules/") if re.match("^cmd_\w+\.py$",module)]:
			try:
				self.loadModule(moduleName[:-3])
			except:
				self.reportError("Error importing module: %s"%moduleName)
	
	def unloadAllModules(self):
		""" Unloads every module that's loaded """
		for moduleName in self.core.ram.get("bot","modules").keys()[:]:
			self.unloadModule(moduleName)

	def reloadAllModules(self):
		""" Reloads every module that's loaded but does not unset variables """
		for moduleName in self.core.ram.get("bot","modules").keys()[:]:
			self.reloadModule(moduleName)

	def resetAllModules(self):
		""" Reloads every module that's loaded and resets them """
		error = 0
		for moduleName in self.core.ram.get("bot","modules").keys()[:]:
			if not self.resetModule(moduleName): error = 1
		return error

########################
# Bot Stuff
	def hasAccess(self,prefix,level):
		if not level: return 1
		for user in settings.accessList:
			if re.compile(user).match(prefix) and settings.accessList[user] >= level:
				return 1

	def say(self,dest,message):
		""" This says 'message' to 'dest' """
		if not message.startswith("/"):
			message = "/notice %s %s"%(dest["user"],message)
		elif message.split()[0] == "/all": message = " ".join(message.split()[1:])
		elif message.split()[0] == "/pm":
			message = " ".join(message.split()[1:])
			dest["channel"] = dest["user"]
		
		netout.handle(self.core,dest,message)

	def process(self,prefix,source,split):
		""" This processes an incoming message and sends it to the appropriate bot commands """
		command = split.pop(0)
		if command.startswith("-"):
			command = command[1:]
			if command in self.core.ram.get("bot","commands"):
				info = self.core.ram.get("bot","commands")[command]
				# Check if we have access
				if self.hasAccess(prefix,info[2]):
					# Check if it's not banned
					if not source["channel"].lower() in info[3]:
						source["prefix"] = prefix
						thread.start_new_thread(self.runCommand,(command,info[0],source,split))
					else:
						if info[4]: self.say(source,"This command is banned here: %s"%info[4])
						else: self.say(source,"This command is banned in this channel.")
				else:
					self.say(source,"Nope")
				return 1
					
	def runCommand(self,name,command,source,split):
		""" This is just a function for running a command in the same thread as it so we can catch errors """
		try:
			command(self.core,self,source,split)
		except:
			self.reportError("Error running command: %s, [%s], [%s]"%(name,", ".join(split),", ".join(source.values())))
